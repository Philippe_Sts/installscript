sudo apt update
sudo apt upgrade

# C and C++ tools
sudo apt install build-essential git ccache cmake gdb valgrind kcachegrind massif-visualizer vim make binutils flex byacc bison libxcb1-dev cloc libboost-all-dev libboost-dev libc++1 bless

# ASM and OS tools
sudo apt install nasm qemu qemu-system-x86_64

# VSCode
sudo apt update
sudo apt install software-properties-common apt-transport-https curl
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt update
sudo apt install code

cd CodePlugins
code --install-extension cpptools-linux.vsix
code --install-extension ms-vscode.cpptools-themes-1.0.0.vsix
code --install-extension jeff-hykin.better-cpp-syntax-1.15.5.vsix
code --install-extension GitHub.vscode-pull-request-github-0.22.0.vsix
code --install-extension darktears.dark-c-plus-plus-theme-0.3.0.vsix
cd ../..

git clone https://Philippe_Sts@bitbucket.org/Philippe_Sts/formation_c-cpp.git