# README #

### How do I get set up? ###

### 1) Download : ###
https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.7.0-amd64-xfce-CD-1.iso

### 2) Install debian ###
### 3) Install git ###

sudo apt install git

if user not in sudoers : 

*$ su*

*enter root password*

*# nano /etc/sudoers*

find the line :

*root ALL=(ALL:ALL) ALL*

enter this afterward (replace USERNAME by your username):

*USERNAME ALL=(ALL:ALL) ALL*

*# exit*

*$ sudo apt install git*


### 4) Download this script on your new debian install ###

git clone https://Philippe_Sts@bitbucket.org/Philippe_Sts/installscript.git

### 5) Execute the install script ###

./installScript.sh